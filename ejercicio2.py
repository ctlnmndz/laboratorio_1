#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random

# Función para crear la matriz
def crear_matriz_aleatoria(filas,columnas):

    i = 0
    j = 0
    matriz = []

    for i in range(filas):
          matriz.append([0]*columnas)

    for i in range(filas):
        for j in range(columnas):
            matriz[i][j] = random.randint(-10,10)

    for i in matriz:
        print(i)

    # Se llaman las funciones
    elemento_menor(filas,columnas,matriz)
    suma_filas(filas,columnas,matriz)
    elementos_negativos(filas,columnas,matriz)

# Función para buscar el número menor de la matriz
def elemento_menor(filas,columnas,matriz):

    numero_menor = 30

    for i in range(filas):
        for j in range(columnas):
            if matriz[i][j] < numero_menor:
                numero_menor = matriz[i][j]

    print("El numero menor es:",numero_menor)

# Función que suma filas
def suma_filas(filas,columnas,matriz):

    suma = 0
    suma_temp = 0

    for i in range(filas):
        for j in range(columnas):
            # Suma solo en las primeras 5 filas
            if i < 5:
                for i in range(filas):
                    for j in range(columnas):
                        if j == 0:
                            suma = matriz[i][j]
                        else :
                            suma = suma + matriz[i][j]
                suma_temp = suma_temp + suma

    print("La suma de las primeras 5 columnas es:",suma_temp)

# Función que busca números negativos
def elementos_negativos(filas,columnas,matriz):

    numeros_negativos = 0

    for i in range(filas):
        for j in range(columnas):
            # busca números negativos sólo la columna 5,6,7,8,9
            if j >= 5 and j <= 9:
                if matriz[i][j] < 0:
                    numeros_negativos=numeros_negativos + 1

    print("La cantidad de numero negativos que estan entre la fila 5 y 9 es:",numeros_negativos)

# función principal
if __name__ == "__main__":

    filas = 15
    columnas = 12

    crear_matriz_aleatoria(filas,columnas)
