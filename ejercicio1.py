#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Función para cambiar la lista
def cambiar_orden(lista):

    numero = 0

    for i in range(len(lista)):
        if i == 0:
            # Se le da el valor del primer elemento
            numero = lista[i]
            # se elimina el primer elemnto
            del lista[i]
    # Se le añade el primer elemento al final
    lista.append(numero)

    print(lista)

# Función principal
if __name__ == "__main__":

    tupla = (1,2,3,4,5,6,7,8,9)
    lista = []
    # Se convierte la tupla en lista
    lista = list(tupla)

    print(lista)

    cambiar_orden(lista)
